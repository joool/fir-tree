// Print a fir tree to stdout
//
// Compile:
//    gcc -Wall --pedantic fir_tree.c -o fir_tree
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, const char* argv[] )
{
  if (argc < 2) {
    printf("Usage: %s <height>", argv[0]);
    exit(1);
  }

  int height = atoi( argv[1] );

  // lets get a buffer to hold our tree
  // init to whitespace, so we can save the effort to pad later
  // size is h*(h+1) for the left side of the tree (including trunk),
  // ((h-1)^2 + h-1)/2 for the right hand branches
  // and h for newline chars on every line
  int h_1 = height - 1;
  int buffersize = height * (height + 1) + ((h_1*h_1) + h_1)/2 + height;
  char buffer[buffersize + 1]; // one extra for NULL
  memset(buffer, ' ', sizeof buffer - 1);
  buffer[sizeof buffer - 1] = '\0';

  // branches
  int p = 0;
  for (int i = 1; i <= height; i++) {
    int branchwidth = 2 * i - 1;
    int whitespacewidth = height - i;

    p += whitespacewidth;
    for (int j = 0; j < branchwidth; j++) {
      buffer[p++] = '#';
    }

    buffer[p++] = '\n';
  }

  // trunk
  p += height - 1;
  buffer[p++] = 'I';
  buffer[p++] = '\n';

  printf(buffer);
}
