"""Everyone needs a chrismas tree."""
import sys


def create_tree(height: int) -> str:
    r"""Return tree string.

    >>> create_tree(3)
    '  #\n ###\n#####\n  I'
    """
    tree = ""

    # branches
    width = 2 * height - 1
    for i in range(height):
        treewidth = 2 * i + 1
        branch = "#" * treewidth
        whitespace = (width - treewidth) // 2
        tree += " " * whitespace + branch + "\n"

    # trunk
    tree += " " * (width - height) + "I"
    return tree


if __name__ == "__main__":
    print(create_tree(int(sys.argv[1])))
