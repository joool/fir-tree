/*
 * Print a fir tree to stdout.
 */
import create_tree from './modules/fir_tree.mjs';
console.log(create_tree(parseInt(process.argv[2])))
