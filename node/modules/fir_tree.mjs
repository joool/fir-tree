
const create_tree = (height) => {
    let tree = "";

    //  Branches
    const width = Math.max(0, 2 * height - 1);
    for (let i = 0; i < height; i++) {
        const treewidth = 2 * i + 1;
        const branch = "#".repeat(treewidth);
        const whitespace = Math.floor((width - treewidth) / 2);
        tree += `${" ".repeat(whitespace) + branch}\n`;
    }

    //  Trunk
    tree += `${" ".repeat(width - height)}I`;

    return tree;
};

export default create_tree
