import create_tree from '../modules/fir_tree.mjs';
import assert from 'assert'

describe('Fir Tree', function() {
  it('should render 0 height', function() {
    assert.equal(create_tree(0), "I");
  });

  it('should render 1 height', function() {
    assert.equal(create_tree(1), "#\nI");
  });

  it('should render 4 height', function() {
    assert.equal(create_tree(4), "   #\n  ###\n #####\n#######\n   I");
  });
});
