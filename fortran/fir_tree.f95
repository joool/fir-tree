! Print a fir tree to stdout.
!
! Bug 1: for some reason, Fortran eats the first char?
! Bug 2: Fortran puts a whitespace in front of output, compensating Bug 1...
!
program Fir_tree
  implicit none
  integer(kind=8) :: h_1, buffersize, height, p, i, j, branchwidth, whitespacewidth;
  character(32) :: arg
  character(len=:), allocatable :: tree(:)

  call get_command_argument(1, arg)
  read(arg,*)height

  ! lets get a buffer to hold our tree
  ! init to whitespace, so we can save the effort to pad later
  ! size is h*(h+1) for the left side of the tree (including trunk),
  ! ((h-1)^2 + h-1)/2 for the right hand branches
  ! and h for newline chars on every line
  h_1 = height - 1;
  buffersize = height * (height + 1) + ((h_1*h_1) + h_1)/2 + height;
  allocate(character(1) :: tree(buffersize))

  ! branches
  p = 0;
  do i = 1, height
    branchwidth = 2 * i - 1;
    whitespacewidth = height - i;
    do j =  1, whitespacewidth
      tree(p) = ' ';
      p = p + 1;
    end do

    do j = 1, branchwidth
      tree(p) = '#';
      p = p + 1;
    end do

    tree(p) = NEW_LINE('A');
    p = p + 1;
  end do

  ! trunk
  do j = 1, height-1
    tree(p) = ' ';
    p = p + 1;
  end do

  tree(p) = 'I';
  print *, tree;

end program Fir_tree
