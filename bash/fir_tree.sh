#!/bin/bash
#
# Print a fir tree of input height to stdout
# Usage: ./fir_tree.sh 5
#
set -euo pipefail
IFS=$'\n\t'

height=$1

# branches
for i in $(seq 1 "$height"); do
  branchwidth=$((2 * i - 1))
  whitespacewidth=$((height - i))

  for _ in $(seq 1 $whitespacewidth); do printf " "; done
  for _ in $(seq 1 $branchwidth);     do printf "#"; done

  printf "\n"
done

# trunk
for _ in $(seq 2 "$height"); do printf " "; done
printf "I\n"
